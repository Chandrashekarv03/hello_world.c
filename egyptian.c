#include <stdio.h>
#include <math.h>
typedef struct vertex
{
      float x, y;
} vertex;

typedef struct rectangle
{
      float length;
      float breadth;
      float area;
      vertex v[3];
} rectangle;

rectangle input_rectangle()
{
      rectangle r;
      printf("Enter the vertices of the rectangle: \n");
      scanf("%f%f%f%f%f%f", &r.v[0].x, &r.v[0].y, &r.v[1].x, &r.v[1].y, &r.v[2].x, &r.v[2].y);
      return r;
}

void input_n_rectangles(int n, rectangle ar[n])
{
      for (int i = 0; i < n; i++)
      {
            ar[i] = input_rectangle();
      }
}

void compute_one_rectangle(rectangle *pr)
{
      pr->length = sqrt(pow(pr->v[0].x - pr->v[1].x, 2) + (pow(pr->v[0].x - pr->v[1].y, 2)));
      pr->breadth = sqrt(pow(pr->v[1].x - pr->v[2].x, 2) + (pow(pr->v[1].x - pr->v[2].y, 2)));
      pr->area = pr->length * pr->breadth;
}

void compute_n_rectangles(int n, rectangle ar[n])
{
      for (int i = 0; i < n; i++)
      {
            compute_one_rectangle(&ar[i]);
      }
}

void output_one_rectangle(rectangle r)
{
      printf("area of rectangle is %0.2f with vertices (%0.2f,%0.2f), (%0.2f,%0.2f) and(%0.2f,%0.2f) \n",
             r.area, r.v[0].x, r.v[0].y, r.v[1].x, r.v[1].y, r.v[2].x, r.v[2].y);
}

void output_n_rectangles(int n, rectangle ar[n])
{
      for (int i = 0; i < n; i++)
      {
            output_one_rectangle(ar[i]);
      }
}

int main()
{
      int n;
      printf("Enter the number of rectangles \n");
      scanf("%d", &n);
      rectangle ar[n];
      input_n_rectangles(n, ar);
      compute_n_rectangles(n, ar);
      output_n_rectangles(n, ar);
      return 0;
}